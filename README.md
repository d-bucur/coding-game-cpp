Contains the solution I used on the [Mars Lander](https://www.codingame.com/training/easy/mars-lander-episode-1) challenge over at codingame.

The solution utilizes a [PID controller](https://en.wikipedia.org/wiki/PID_controller) and solves episodes 1, 2 with 100% score, and gets a 
50% score in episode 3 because it is missing a waypoint system, which is a potential improvement.