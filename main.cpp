#include <utility>

#include <utility>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
using namespace std;

struct Vector2 {
    float x=0, y=0;

    Vector2 operator+(const Vector2& rhl) const {
        return Vector2{x+rhl.x, y+rhl.y};
    }
    Vector2 operator+=(const Vector2& rhl) {
        x += rhl.x;
        y += rhl.y;
        return *this;
    }
    Vector2 operator-(const Vector2& rhl) const {
        return Vector2{x-rhl.x, y-rhl.y};
    }
    Vector2 operator*(float f) const {
        return Vector2{x*f, y*f};
    }
    Vector2 operator/(float f) const {
        return Vector2{x/f, y/f};
    }
    inline float sqrMagnitude() const {
        return x*x + y*y;
    }
    inline float magnitude() const {
        return sqrt(x*x + y*y);
    }
    Vector2 normalized() const {
        auto mag = sqrt(sqrMagnitude());
        return Vector2{x,y} / mag;
    }
    Vector2 clampMagnitude(const float maxMag) const {
        if (sqrMagnitude() > maxMag * maxMag)
            return normalized() * maxMag;
        return *this;
    }
};

ostream &operator<<(ostream &os, Vector2 const &v) {
    return os << v.x << ", " << v.y << endl;
}

struct Line {
    Vector2 p1, p2;
};

float lerp(float x, Vector2 p0, Vector2 p1) {
    if (p0.y > p1.y) swap(p0, p1);
    return max(float(p0.y), min(p0.y + (x - p0.x) * (p1.y - p0.y) / (p1.x - p0.x), float(p1.y)));
}

struct World {
    Vector2 pos;
    Vector2 speed;
    Vector2 destination;
    int fuel;
    int rotate;
    int power;
};

Vector2 makeAccelerationVector(float rotate, float power) {
    auto normalizedRotation = (rotate + 90) * M_PI/180.0f;
    return Vector2 {
        float(cos(normalizedRotation)),
        float(sin(normalizedRotation))
    } * power;
}

World simulateTick(const World& world, int times=1) {
    const float MARS_GRAVITY = -3.711f;
    World prev = world;
    World next;
    for (int i=0; i<times; ++i) {
        next = prev;
        next.pos = prev.pos + prev.speed;
        auto accel = Vector2{0, MARS_GRAVITY} + makeAccelerationVector(prev.rotate, prev.power);
        next.speed = prev.speed + accel;
        next.fuel -= prev.power;
        //next.power = max (prev.power - 1, 0);
        prev = next;
    }
    return next;
}

struct PIDController {
    PIDController(float KP, float KI, float KD, float KH, int derivateTicks, Vector2 (*calculateF)(const World&), float maxFeedbackLength) {
        this->KP = KP;
        this->KI = KI;
        this->KD = KD;
        this->KH = KH;
        this->derivateTicks = derivateTicks;
        this->calculateError = calculateF;
        this->maxFeedbackLength = maxFeedbackLength;
    }

    float KP, KD, KI, KH;
    int derivateTicks;
    Vector2 historicError = {0, 0};
    Vector2 (*calculateError)(const World&);
    bool debug = false;
    float maxFeedbackLength;

    Vector2 feedbackLoop(const World &world) {
        auto error = calculateError(world);

        auto proportional = error * KP;

        auto integral = historicError * KI;
        historicError = historicError * (1-KH) + error * KH;

        // TODO calculate derivate as extrapolation of historic movement to avoid frequent rotation changes
        auto next = simulateTick(world, derivateTicks);
        auto derivate = calculateError(next) * KD;

        if (debug) {
            cerr << "Error: " << error;
            cerr << "Proportional: " << proportional;
            cerr << "Integral: " << integral;
            cerr << "Derivate: " << derivate;
            //cerr << "Predicted pos: " << next.pos;
        }

        return (proportional + integral + derivate) * -1;
    }

    Vector2 clampedFeedbackLoop(const World &world) {
        return feedbackLoop(world)
            .clampMagnitude(maxFeedbackLength)
            / maxFeedbackLength;
    }
};

template <typename T> float sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

float truncateLowerVals(float x, float min) {
    return abs(x) > min ? (abs(x) - min) * sgn(x) : 0;
}

int calcPower(const Vector2 v) {
    const int MAX_POWER = 4;
    auto n = v;
    n.y = max(0.0f, v.y);
    //cerr << "Modified power vec: " << n;
    auto y = n.magnitude();
    //cerr << "magnitude used: " << y;
    if (y < 0.05f)
        return 0;
    if (y < 0.25f)
        return 1;
    if (y < 0.5f)
        return 2;
    if (y < 0.75f)
        return 3;
    return 4;
}

class LanderAI {
    const int MAX_ROTATION = 90;

public:
    explicit LanderAI(vector<Vector2> landingPoints) {
        this->terrainPoints = std::move(landingPoints);
        calculateLanding();
        pid.debug = false;
    }

    Vector2 getLandingPoint() const {
        return landingPoint;
    }

private:
    vector<Vector2> terrainPoints;
    Vector2 landingPoint;
    Line landingLine;
    PIDController pid = PIDController(
        5, 5, 15, 0.3f, 30,
        [](const World& world) -> Vector2 {
            auto pos = world.pos - world.destination;
            // TODO check if speed at destination point is 0 for overshoot on test5
            auto speed = Vector2{
                    truncateLowerVals(world.speed.x, 15),
                    truncateLowerVals(world.speed.y, 30)
            } * 100;
            //cerr << "pos: " << pos;
            //cerr << "speed: " << speed;
            return pos + speed;
        },
        5000
    );

public:
    pair<int, int> calcOutput(World world) {
        Vector2 closestLandingPoint = getClosestLandingPoint(world);
        //fprintf(stderr, "Actual destination is: %f, %f\n", closestLandingPoint.x, closestLandingPoint.y);
        world.destination = getNextDestination(world);

        int newRotation, newPower;

        if ((world.pos - closestLandingPoint).magnitude() < 200) {
            cerr << "Landing procedure activated";
            newRotation = 0;
            newPower = 3 + (world.fuel%2);
            // TODO start destination with a different PID for smoother destination on test5
        } else {
            auto feedback = pid.clampedFeedbackLoop(world);
            cerr << "Feedback: " << feedback;

            newRotation = feedback.x * -MAX_ROTATION;
            newPower = calcPower(feedback);
        }
        return {newRotation, newPower};
    }

private:
    // TODO rethink if useful
    Vector2 getClosestLandingPoint(const World &world) const {
        return {min(max(world.pos.x, landingLine.p1.x), landingLine.p2.x), landingLine.p2.y};
    }

    Vector2 getNextDestination(const World world) const {
        vector<Vector2> filteredPoints;
        copy_if(terrainPoints.begin(),terrainPoints.end(), back_inserter(filteredPoints),
             [&](Vector2 p) {
                return p.x < max(world.pos.x, landingPoint.x) && p.x > min(world.pos.x, landingPoint.x);
        });

        if (world.pos.x > landingPoint.x)
            reverse(filteredPoints.begin(), filteredPoints.end());

        for (auto p : filteredPoints) {
            auto projectedPoint = lerp(p.x, world.pos, landingPoint);
            if (projectedPoint < p.y) {
                cerr << "Edge: " << p;
                cerr << "Intermediate: " << p + Vector2{0, 1000};
                return p + Vector2{0, 6000}; // TODO waay too Y, but needed
            }
        }
        return landingPoint;
    }

    void calculateLanding() {
        Vector2 previousLand {-100, -100};
        landingLine = {{-1,-1}, {-1,-11}};
        for (auto p : terrainPoints) {
            if (p.y == previousLand.y) {
                landingLine.p1 = previousLand;
                landingLine.p2 = p;
            }
            previousLand = p;
        }
        // add vertical offset to destination
        landingLine.p1.y += 200;
        landingLine.p2.y += 200;

        if (landingLine.p1.y == -1)
            fprintf(stderr, "No destination zone found\n");
        else
            fprintf(stderr, "Landing zone is: %f, %f; %f, %f\n", landingLine.p1.x, landingLine.p1.y, landingLine.p2.x, landingLine.p2.y);

        landingPoint = {(landingLine.p1.x + landingLine.p2.x)/2, landingLine.p2.y};
        fprintf(stderr, "Landing point is: %f, %f\n", landingPoint.x, landingPoint.y);
    }
};

int main()
{
    int surfaceN; // the number of points used to draw the surface of Mars.
    vector<Vector2> landPoints;
    cin >> surfaceN; cin.ignore();
    for (int i = 0; i < surfaceN; i++) {
        Vector2 currentLand; // X coordinate of a surface point. (0 to 6999)
        // Y coordinate of a surface point. By linking all the points together in a sequential fashion, you form the surface of Mars.
        cin >> currentLand.x >> currentLand.y;
        cin.ignore();
        landPoints.push_back(currentLand);
    }

    auto lander = LanderAI(landPoints);
    // TODO better encapsulation of landingpoint
    auto landingPoint = lander.getLandingPoint();

    // game loop
    while (true) {
        int X;
        int Y;
        int hSpeed; // the horizontal speed (in m/s), can be negative.
        int vSpeed; // the vertical speed (in m/s), can be negative.
        int fuel; // the quantity of remaining fuel in liters.
        int rotate; // the rotation angle in degrees (-90 to 90).
        int power; // the thrust power (0 to 4).
        cin >> X >> Y >> hSpeed >> vSpeed >> fuel >> rotate >> power; cin.ignore();

        World world{{float(X), float(Y)}, {float(hSpeed), float(vSpeed)}, landingPoint, fuel, rotate, power};
        auto output = lander.calcOutput(world);

        // rotate power. rotate is the desired rotation angle. power is the desired thrust power.
        printf("%d %d\n", output.first, output.second);
    }
}
#pragma clang diagnostic pop